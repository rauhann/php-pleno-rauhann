<?php

namespace Tests\Feature\Seller;

use App\Entities\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class SaveSellerFTest extends TestCase
{
    use DatabaseTransactions;

    private $auth;

    public function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create([
            'password' => Hash::make('123123')
        ]);

        $response = $this->post('/api/login', [
            'email' => $user->email,
            'password' => '123123'
        ]);

        $arrayResponse = json_decode($response->getContent(), true);

        $this->auth = [
            'Authorization' => 'Bearer ' . $arrayResponse['access_token']
        ];
    }

    /**
     * Criar um vendedor com sucesso
     *
     * @return void
     */
    public function test_CreateSellerSuccess()
    {
        $data = [
            'name' => 'Rauhann',
            'email' => 'rauhann2711@gmail.com',
        ];

        $response = $this->post('/api/sellers/create', $data, $this->auth);

        $arrayResponse = json_decode($response->getContent(), true);

        $response->assertStatus(201);

        $this->assertDatabaseHas('sellers', [
            'uuid' => $arrayResponse['data'],
            'name' => $data['name'],
            'email' => $data['email']
        ]);

        $this->assertEquals($arrayResponse['message'], 'Vendedor criado com sucesso');
    }

    /**
     * Criar um vendedor sem nome
     *
     * @return void
     */
    public function test_CreateSellerWithoutName()
    {
        $data = [
            'email' => 'rauhann2711@gmail.com'
        ];

        $response = $this->post('/api/sellers/create', $data, $this->auth);

        $arrayResponse = json_decode($response->getContent(), true);

        $response->assertStatus(422);

        $this->assertDatabaseMissing('sellers', [
            'email' => $data['email']
        ]);

        $this->assertEquals($arrayResponse['errors']['name'][0], 'O campo name é obrigatório');
    }

    /**
     * Criar um vendedor sem email
     *
     * @return void
     */
    public function test_CreateSellerWithoutEmail()
    {
        $data = [
            'name' => 'Rauhann',
        ];

        $response = $this->post('/api/sellers/create', $data, $this->auth);

        $arrayResponse = json_decode($response->getContent(), true);

        $response->assertStatus(422);

        $this->assertDatabaseMissing('sellers', [
            'name' => $data['name']
        ]);

        $this->assertEquals($arrayResponse['errors']['email'][0], 'O campo email é obrigatório');
    }
}
