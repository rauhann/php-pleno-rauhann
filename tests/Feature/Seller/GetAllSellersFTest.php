<?php

namespace Tests\Feature\Seller;

use App\Entities\Seller;
use App\Entities\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class GetAllSellersFTest extends TestCase
{
    use DatabaseTransactions;

    private $auth;

    public function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create([
            'password' => Hash::make('123123')
        ]);

        $response = $this->post('/api/login', [
            'email' => $user->email,
            'password' => '123123'
        ]);

        $arrayResponse = json_decode($response->getContent(), true);

        $this->auth = [
            'Authorization' => 'Bearer ' . $arrayResponse['access_token']
        ];
    }

    /**
     * Retorna todos os vendedores
     *
     * @return void
     */
    public function test_listSellersSuccess()
    {
        $numberOfSellers = 4;

        Seller::factory()->count($numberOfSellers)->create();

        $response = $this->get('/api/sellers/list', $this->auth);

        $arrayResponse = json_decode($response->getContent(), true);

        $response->assertStatus(200);

        // $this->assertCount($numberOfSellers, $arrayResponse['data']);
    }

    /**
     * Retorna nenhum vendedor
     *
     * @return void
     */
    public function test_listSellersEmpty()
    {
        $response = $this->get('/api/sellers/list', $this->auth);

        $arrayResponse = json_decode($response->getContent(), true);

        $response->assertStatus(200);

        // $this->assertCount(0, $arrayResponse['data']);
    }
}
