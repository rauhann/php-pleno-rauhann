<?php

namespace Tests\Feature\User;

use App\Entities\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class LoginUserFTest extends TestCase
{
    use DatabaseTransactions;

     /**
     * Loga um usuário
     *
     * @return void
     */
    public function test_LoginUserSuccess()
    {
        $user = User::factory()->create([
            'password' => Hash::make('123123')
        ]);

        $response = $this->post('/api/login', [
            'email' => $user->email,
            'password' => '123123'
        ]);

        $arrayResponse = json_decode($response->getContent(), true);

        $response->assertStatus(200);

        $this->assertArrayHasKey('access_token', $arrayResponse);
    }

    /**
     * Tenta logar um usuário com senha inválida
     *
     * @return void
     */
    public function test_LoginUserWithPasswordInvalid()
    {
        $user = User::factory()->create([
            'password' => Hash::make('123123')
        ]);

        $response = $this->post('/api/login', [
            'email' => $user->email,
            'password' => '123456'
        ]);

        $response->assertStatus(401);
    }
}
