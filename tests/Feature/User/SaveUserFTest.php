<?php

namespace Tests\Feature\User;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class SaveUserFTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Criar um usuário com sucesso
     *
     * @return void
     */
    public function test_CreateUserSuccess()
    {
        $data = [
            'name' => 'Rauhann',
            'email' => 'rauhann2711@gmail.com',
            'password' => '123123'
        ];

        $response = $this->post('/api/users/create', $data);

        $arrayResponse = json_decode($response->getContent(), true);

        $response->assertStatus(201);

        $this->assertDatabaseHas('users', [
            'uuid' => $arrayResponse['data'],
            'name' => $data['name'],
            'email' => $data['email']
        ]);

        $this->assertEquals($arrayResponse['message'], 'Usuário criado com sucesso');
    }

    /**
     * Criar um usuário sem nome
     *
     * @return void
     */
    public function test_CreateUserWithoutName()
    {
        $data = [
            'email' => 'rauhann2711@gmail.com',
            'password' => '123123'
        ];

        $response = $this->post('/api/users/create', $data);

        $arrayResponse = json_decode($response->getContent(), true);

        $response->assertStatus(422);

        $this->assertDatabaseMissing('users', [
            'email' => $data['email']
        ]);

        $this->assertEquals($arrayResponse['errors']['name'][0], 'O campo name é obrigatório');
    }

    /**
     * Criar um usuário sem email
     *
     * @return void
     */
    public function test_CreateUserWithoutEmail()
    {
        $data = [
            'name' => 'Rauhann',
            'password' => '123123'
        ];

        $response = $this->post('/api/users/create', $data);

        $arrayResponse = json_decode($response->getContent(), true);

        $response->assertStatus(422);

        $this->assertDatabaseMissing('users', [
            'name' => $data['name']
        ]);

        $this->assertEquals($arrayResponse['errors']['email'][0], 'O campo email é obrigatório');
    }

    /**
     * Criar um usuário sem senha
     *
     * @return void
     */
    public function test_CreateUserWithoutPassword()
    {
        $data = [
            'name' => 'Rauhann',
            'email' => 'rauhann2711@gmail.com',
        ];

        $response = $this->post('/api/users/create', $data);

        $arrayResponse = json_decode($response->getContent(), true);

        $response->assertStatus(422);

        $this->assertDatabaseMissing('users', [
            'name' => $data['name']
        ]);

        $this->assertEquals($arrayResponse['errors']['password'][0], 'O campo password é obrigatório');
    }
}
