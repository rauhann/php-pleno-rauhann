<?php

namespace Tests\Feature\Sale;

use App\Entities\Seller;
use App\Entities\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class SaveSaleFTest extends TestCase
{
    use DatabaseTransactions;

    private $auth;

    public function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create([
            'password' => Hash::make('123123')
        ]);

        $response = $this->post('/api/login', [
            'email' => $user->email,
            'password' => '123123'
        ]);

        $arrayResponse = json_decode($response->getContent(), true);

        $this->auth = [
            'Authorization' => 'Bearer ' . $arrayResponse['access_token']
        ];
    }

    /**
     * Criar uma venda com sucesso
     *
     * @return void
     */
    public function test_CreateSaleSuccess()
    {
        $seller = Seller::factory()->create();

        $data = [
            'value' => 2000
        ];

        $response = $this->post('/api/sales/create/' . $seller->uuid, $data, $this->auth);

        $arrayResponse = json_decode($response->getContent(), true);

        $response->assertStatus(201);

        $this->assertDatabaseHas('sales', [
            'uuid' => $arrayResponse['data'],
            'value' => $data['value'],
        ]);

        $this->assertEquals($arrayResponse['message'], 'Venda criada com sucesso');
    }

    /**
     * Criar uma venda com vendedor inexistente
     *
     * @return void
     */
    public function test_CreateSaleWithoutSeller()
    {
        $data = [
            'value' => 2000
        ];

        $response = $this->post('/api/sales/create/sdsd23', $data, $this->auth);

        $arrayResponse = json_decode($response->getContent(), true);

        $response->assertStatus(400);

        $this->assertDatabaseMissing('sales', [
            'value' => $data['value']
        ]);

        $this->assertEquals($arrayResponse['message'], 'Vendedor não encontrado');
    }

    /**
     * Criar uma venda sem valor
     *
     * @return void
     */
    public function test_CreateSaleWithoutValue()
    {
        $seller = Seller::factory()->create();

        $data = [];

        $response = $this->post('/api/sales/create/' . $seller->uuid, $data, $this->auth);

        $arrayResponse = json_decode($response->getContent(), true);

        $response->assertStatus(422);

        $this->assertEquals($arrayResponse['errors']['value'][0], 'O campo value é obrigatório');
    }
}
