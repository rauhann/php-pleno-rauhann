<?php

namespace Tests\Feature\Sale;

use App\Entities\Sale;
use App\Entities\Seller;
use App\Entities\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class GetSalesFTest extends TestCase
{
    use DatabaseTransactions;

    private $auth;

    public function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create([
            'password' => Hash::make('123123')
        ]);

        $response = $this->post('/api/login', [
            'email' => $user->email,
            'password' => '123123'
        ]);

        $arrayResponse = json_decode($response->getContent(), true);

        $this->auth = [
            'Authorization' => 'Bearer ' . $arrayResponse['access_token']
        ];
    }

    /**
     * Lista as vendas de um vendedor pelo seu id
     *
     * @return void
     */
    public function test_listSaleByUuidSellerSuccess()
    {
        $numberSales = 3;

        $seller = Seller::factory()->create();

        Sale::factory()->count($numberSales)->create([
            'seller_id' => $seller->id
        ]);

        $response = $this->get('/api/sales/list/' . $seller->uuid, $this->auth);

        $arrayResponse = json_decode($response->getContent(), true);

        $response->assertStatus(200);

        $this->assertCount($numberSales, $arrayResponse['data']['sales']);

        $this->assertEquals($arrayResponse['message'], 'Vendas encontradas com sucesso');
    }

    /**
     * Lista as vendas de um vendedor pelo seu id sem vendas
     *
     * @return void
     */
    public function test_listSaleByUuidSellerWithoutSale()
    {
        $seller = Seller::factory()->create();

        $response = $this->get('/api/sales/list/' . $seller->uuid, $this->auth);

        $arrayResponse = json_decode($response->getContent(), true);

        $response->assertStatus(200);

        $this->assertEquals($arrayResponse['message'], 'Vendas encontradas com sucesso');
    }
}
