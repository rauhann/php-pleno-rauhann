#!make
include .env.db
export $(shell sed 's/=.*//' .env.db)

t=

up:
	docker-compose up
upd:
	docker-compose up -d
down:
	docker-compose down
downs:
	docker-compose down --remove-orphans
build:
	docker-compose up --build
buildd:
	docker-compose up --build -d
sh:
	docker exec -it vendas-app /bin/bash
db:
	docker exec -it vendas-db bash -c "mysql -u root -p'${MYSQL_ROOT_PASSWORD}' ${MYSQL_DATABASE}"
migrate:
	php artisan migrate
install:
	composer install
dum:
	composer dump-autoload
cache:
	php artisan cache:clear && php artisan config:cache && php artisan route:clear && php artisan route:cache
setup:
	cp -r .env.example .env && cp -r .env.db.example .env.db
