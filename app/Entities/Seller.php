<?php

namespace App\Entities;

use App\Models\Seller as ModelsSeller;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Seller extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'sellers';

    protected $fillable = [
        'uuid',
        'name',
        'email'
    ];

    protected $appends = [
        'commission'
    ];

    /**
     * É acionada toda vez que usa essa entidade. Varia de acordo com a ação que é executada.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->uuid = (string) Str::uuid();
        });
    }

    /**
     * Variável lógica comissão
     *
     * @return void
     */
    public function getCommissionAttribute()
    {
        return number_format(ModelsSeller::SELLER_COMMISSION * 100, 2, ',', '.') . '%';
    }

    public function sales()
    {
        return $this->hasMany(Sale::class);
    }
}
