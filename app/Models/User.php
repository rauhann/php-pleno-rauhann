<?php

namespace App\Models;

use App\Entities\User as EntitiesUser;
use Illuminate\Support\Facades\Hash;

class User
{
    private $entityUser;

    public function __construct(EntitiesUser $entityUser)
    {
        $this->entityUser = $entityUser;
    }

    /**
     * Cria um usuário para acesso a api.
     *
     * @param string $name
     * @param string $email
     * @param string $password
     * @return void
     */
    public function saveUser(
        string $name,
        string $email,
        string $password
    ): string
    {
        $user = $this->entityUser->create([
            'name' => $name,
            'email' => $email,
            'password' => Hash::make($password)
        ]);

        return $user->uuid;
    }
}
