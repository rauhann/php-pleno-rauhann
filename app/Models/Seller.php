<?php

namespace App\Models;

use App\Entities\Seller as EntitiesSeller;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Cache;

class Seller
{
    const SELLER_COMMISSION = 0.065; // 6.5%

    private $entitySeller;

    public function __construct(EntitiesSeller $entitySeller)
    {
        $this->entitySeller = $entitySeller;
    }

    /**
     * Cria um vendedor.
     *
     * @param string $name
     * @param string $email
     */
    public function saveSeller(
        string $name,
        string $email
    ) {
        $seller = $this->entitySeller->create([
            'name' => $name,
            'email' => $email
        ]);

        return [
            'id' => $seller->uuid,
            'name' => $seller->name,
            'email' => $seller->email
        ];
    }

    /**
     * Lista todos os vendedores
     */
    public function getAll()
    {
        return Cache::get('getAll', function () {
            $sellers = $this->entitySeller->get(['uuid', 'name', 'email']);

            Cache::store('file')->put('getAll', $sellers, env('CACHE_TIMER'));

            return $sellers;
        });
    }

    /**
     * Lista um vendedor a partir do seu uuid
     *
     * @param string $uuid
     */
    public function getByUuid(string $uuid)
    {
        return $this->entitySeller->where('uuid', $uuid)->first();
    }

    /**
     * Retorna o vendedor e suas vendas
     *
     * @param string $uuid
     */
    public function getByUuidWithSales(string $uuid)
    {
        $seller = $this->entitySeller->with('sales')->where('uuid', $uuid)->first();

        if (!$seller) {
            throw new Exception('Vendedor não encontrado');
        }

        $sales = $this->formatSalesBySeller($seller->sales->toArray());

        return [
            'id' => $seller->uuid,
            'name' => $seller->name,
            'email' => $seller->email,
            'sales' => $sales
        ];
    }

    /**
     * Formata vendas para retorno
     *
     * @param array $sales
     */
    private function formatSalesBySeller(array $sales)
    {
        $result = [];
        foreach ($sales as $sale) {
            $result[] = [
                'value' => number_format($sale['value'], 2, '.', ','),
                'commission' => number_format($sale['commission'], 2, '.', ','),
                'date' => Carbon::parse($sale['created_at'])->format('d/m/Y H:i:s')
            ];
        }

        return $result;
    }
}
