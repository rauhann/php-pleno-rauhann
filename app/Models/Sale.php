<?php

namespace App\Models;

use App\Entities\Sale as EntitiesSale;
use Carbon\Carbon;
use Exception;

class Sale
{
    private $entitySale;
    private $seller;

    public function __construct(
        EntitiesSale $entitySale,
        Seller $seller
    ) {
        $this->entitySale = $entitySale;
        $this->seller = $seller;
    }

    /**
     * Cria uma venda associada a um vendedor
     *
     * @param string $sellerId
     * @param float $value
     * @return array
     */
    public function saveSale(
        string $sellerId,
        float $value
    ): array {
        $seller = $this->seller->getByUuid($sellerId);

        if (!$seller) {
            throw new Exception('Vendedor não encontrado');
        }

        $baseCommission = $this->formatBaseCommission($seller->commission);

        $sale = $this->entitySale->create([
            'seller_id' => $seller->id,
            'value' => $value,
            'base_commission' => $baseCommission,
            'commission' => $this->calculateCommission($baseCommission, $value),
            'date' => date('Y-m-d H:i:s')
        ]);

        return [
            'id' => $sale->uuid,
            'name' => $seller->name,
            'email' => $seller->email,
            'value' => $sale->value,
            'commission' => $sale->commission,
            'date' => Carbon::parse($sale->date)->format('d/m/Y H:i:s')
        ];
    }

    /**
     * Ajuste comissão base para cálculo
     *
     * @param string $baseCommission
     * @return float
     */
    private function formatBaseCommission(string $baseCommission): float
    {
        $aux = preg_replace('/[^0-9,]/', '', $baseCommission);
        $result = str_replace(',', '.', $aux);

        return (float) $result / 100;
    }

    /**
     * Calcula comissao do vendedor
     *
     * @param float $baseCommission
     * @param float $value
     * @return float
     */
    private function calculateCommission(float $baseCommission, float $value): float
    {
        return $value * $baseCommission;
    }
}
