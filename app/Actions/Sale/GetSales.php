<?php

namespace App\Actions\Sale;

use App\Http\Controllers\Controller;
use App\Models\Seller;

class GetSales extends Controller
{
    private $modelSeller;

    public function __construct(Seller $modelSeller)
    {
        $this->modelSeller = $modelSeller;
    }

    /**
     * @OA\Get(
     * path="/api/sales/list/{sellerId}",
     * summary="Lista as vendas de um vendedor",
     * description="Lista as vendas de um vendedor",
     * tags={"Venda"},
     * security={ {"Token": {} }},
     * @OA\Parameter(
     *    description="UUID do vendedor",
     *    in="path",
     *    name="sellerId",
     *    required=true,
     *    example="xxx",
     *    @OA\Schema(
     *       type="string"
     *    )
     * ),
     * @OA\Response(
     *    response=200,
     *    description="Vendas encontradas com sucesso",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Vendas encontradas com sucesso")
     *        )
     *     )
     * )
     */
    public function __invoke(string $sellerId)
    {
        $result = $this->modelSeller->getByUuidWithSales($sellerId);

        return response()->json([
            'data' => $result,
            'message' => 'Vendas encontradas com sucesso'
        ], 200);
    }
}
