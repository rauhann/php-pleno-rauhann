<?php

namespace App\Actions\Sale;

use App\Http\Controllers\Controller;
use App\Http\Requests\SaleRequest;
use App\Models\Sale;

class SaveSale extends Controller
{
    private $modelSale;

    public function __construct(Sale $modelSale)
    {
        $this->modelSale = $modelSale;
    }

    /**
     * @OA\Post(
     * path="/api/sales/create/{sellerId}",
     * summary="Cria uma venda para um vendedor",
     * description="Cria uma venda para um vendedor",
     * tags={"Venda"},
     * security={ {"Token": {} }},
     * @OA\Parameter(
     *    description="UUID do vendedor",
     *    in="path",
     *    name="sellerId",
     *    required=true,
     *    example="xxx",
     *    @OA\Schema(
     *       type="string"
     *    )
     * ),
     * @OA\RequestBody(
     *    required=true,
     *    @OA\JsonContent(
     *       required={"value"},
     *       @OA\Property(property="value", type="float", format="number", example="1000")
     *    ),
     * ),
     * @OA\Response(
     *    response=201,
     *    description="Cadastro feito com sucesso",
     *    @OA\JsonContent(
     *       @OA\Property(property="data", type="object",
     *           @OA\Property(property="id", type="string", example="xxx"),
     *           @OA\Property(property="name", type="string", example="xxx"),
     *           @OA\Property(property="email", type="string", example="xxx"),
     *           @OA\Property(property="value", type="number", example="1"),
     *           @OA\Property(property="date", type="date", example="dd/mm/aaaa h:i:s")
     *       ),
     *       @OA\Property(property="message", type="string", example="Venda criada com sucesso")
     *        )
     *     )
     * )
     */
    public function __invoke(SaleRequest $request, string $sellerId)
    {
        $result = $this->modelSale->saveSale(
            $sellerId,
            $request->value
        );

        return response()->json([
            'data' => $result,
            'message' => 'Venda criada com sucesso'
        ], 201);
    }
}
