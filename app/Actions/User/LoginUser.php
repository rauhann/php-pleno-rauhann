<?php

namespace App\Actions\User;

use App\Http\Controllers\Controller;
use App\Models\Auth as ModelAuth;
use Illuminate\Http\Request;

class LoginUser extends Controller
{
    private $auth;

    public function __construct(ModelAuth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * @OA\Post(
     * path="/api/login",
     * summary="Faz login com usuário criado",
     * description="Faz login com usuário criado",
     * tags={"Autenticação"},
     * @OA\RequestBody(
     *    required=true,
     *    @OA\JsonContent(
     *       required={"email","password"},
     *       @OA\Property(property="email", type="string", format="email", example="teste@teste.com"),
     *       @OA\Property(property="password", type="string", format="password", example="123123")
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="Login efetuado",
     *    @OA\JsonContent(
     *       @OA\Property(property="access_token", type="string", example="xxx"),
     *       @OA\Property(property="token_type", type="string", example="bearer"),
     *       @OA\Property(property="expires_in", type="int", example="3600"),
     *
     *        )
     *     )
     * )
     */
    public function __invoke(Request $request)
    {
        return $this->auth->login($request);
    }
}
