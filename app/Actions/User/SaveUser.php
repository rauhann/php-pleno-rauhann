<?php

namespace App\Actions\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Models\User;

class SaveUser extends Controller
{
    private $modelUser;

    public function __construct(User $modelUser)
    {
        $this->modelUser = $modelUser;
    }

    /**
     * @OA\Post(
     * path="/api/users/create",
     * summary="Cria um usuário para acesso",
     * description="Cria um usuário para acesso",
     * tags={"Autenticação"},
     * @OA\RequestBody(
     *    required=true,
     *    @OA\JsonContent(
     *       required={"name","email","password"},
     *       @OA\Property(property="name", type="string", format="text", example="user"),
     *       @OA\Property(property="email", type="string", format="email", example="teste@teste.com"),
     *       @OA\Property(property="password", type="string", format="password", example="123123")
     *    ),
     * ),
     * @OA\Response(
     *    response=201,
     *    description="Cadastro feito com sucesso",
     *    @OA\JsonContent(
     *       @OA\Property(property="data", type="string", example="xxx"),
     *       @OA\Property(property="message", type="string", example="Usuário criado com sucesso"),
     *        )
     *     )
     * )
     */
    public function __invoke(UserRequest $request)
    {
        $result = $this->modelUser->saveUser(
            $request->name,
            $request->email,
            $request->password
        );

        return response()->json([
            'data' => $result,
            'message' => 'Usuário criado com sucesso'
        ], 201);
    }
}
