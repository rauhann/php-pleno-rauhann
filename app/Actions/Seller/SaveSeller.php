<?php

namespace App\Actions\Seller;

use App\Http\Controllers\Controller;
use App\Http\Requests\SellerRequest;
use App\Models\Seller;

class SaveSeller extends Controller
{
    private $modelSeller;

    public function __construct(Seller $modelSeller)
    {
        $this->modelSeller = $modelSeller;
    }

    /**
     * @OA\Post(
     * path="/api/sellers/create",
     * summary="Cria um vendedor",
     * description="Cria um vendedor",
     * tags={"Vendedor"},
     * security={ {"Token": {} }},
     * @OA\RequestBody(
     *    required=true,
     *    @OA\JsonContent(
     *       required={"name","email"},
     *       @OA\Property(property="name", type="string", format="text", example="user"),
     *       @OA\Property(property="email", type="string", format="email", example="teste@teste.com")
     *    ),
     * ),
     * @OA\Response(
     *    response=201,
     *    description="Cadastro feito com sucesso",
     *    @OA\JsonContent(
     *       @OA\Property(property="data", type="object",
     *           @OA\Property(property="id", type="string", example="xxx"),
     *           @OA\Property(property="name", type="string", example="xxx"),
     *           @OA\Property(property="email", type="string", example="xxx")
     *       ),
     *       @OA\Property(property="message", type="string", example="Vendedor criado com sucesso"),
     *        )
     *     )
     * )
     */
    public function __invoke(SellerRequest $request)
    {
        $result = $this->modelSeller->saveSeller(
            $request->name,
            $request->email
        );

        return response()->json([
            'data' => $result,
            'message' => 'Vendedor criado com sucesso'
        ], 201);
    }
}
