<?php

namespace App\Actions\Seller;

use App\Http\Controllers\Controller;
use App\Models\Seller;

class GetAllSellers extends Controller
{
    private $modelSeller;

    public function __construct(Seller $modelSeller)
    {
        $this->modelSeller = $modelSeller;
    }

    /**
     * @OA\Get(
     * path="/api/sellers/list",
     * summary="Lista todos os vendedores",
     * description="Lista todos os vendedores",
     * tags={"Vendedor"},
     * security={ {"Token": {} }},
     * @OA\Response(
     *    response=200,
     *    description="Listagem feita com sucesso",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Vendedores listados com sucesso"),
     *        )
     *     )
     * )
     */
    public function __invoke()
    {
        $result = $this->modelSeller->getAll();

        return response()->json([
            'data' => $result,
            'message' => 'Vendedores listados com sucesso'
        ], 200);
    }
}
