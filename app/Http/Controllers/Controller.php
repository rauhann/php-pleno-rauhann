<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @OA\Info(
     *      version="1.0.0",
     *      title="Documentação de API - Vendas",
     *      description="Esta documentação proporciona a interação do usuário com a API. O método de autenticação é por JWT",
     *      @OA\Contact(
     *          name="Rauhann Chaves",
     *          email="rauhann2711@gmail.com"
     *      )
     * )
     *
     * @OA\Server(
     *      url=L5_SWAGGER_CONST_HOST
     * )
     *
     *
     * @OA\PathItem(
     *     path="/API"
     * )
     *
     * @OA\SecurityScheme(
     *     securityScheme="Token",
     *     type="http",
     *     scheme="bearer"
     * )
     *
     */
}
