FROM ambientum/php:8.0-nginx

LABEL maintainer "Rauhann Chaves"

WORKDIR /app

COPY --chown=nginx:nginx . /app
COPY --chown=nginx:nginx ./nginx/vhost.conf /etc/nginx/sites/enabled.conf
COPY --chown=nginx:nginx ./nginx/99_overrides.ini /etc/php8/conf.d/

RUN sudo chmod 777 -R /app/bootstrap/cache
RUN sudo chmod 777 -R /app/storage/framework
RUN sudo chmod 777 -R /app/storage/logs

RUN sudo rm -rf /var/cache/apk/*

RUN sudo touch crontab.tmp
RUN sudo chmod 777 crontab.tmp
RUN sudo echo '* * * * * cd /app && php artisan schedule:run >> /dev/null 2>&1' > crontab.tmp
RUN sudo crontab crontab.tmp
RUN sudo rm -rf crontab.tmp

EXPOSE 80
