#!/bin/sh
sleep 10s
sudo /usr/sbin/crond
composer install
php artisan migrate
php artisan l5-swagger:generate
php artisan serve --host=0.0.0.0 --port=80
