<?php

namespace Database\Factories\Entities;

use App\Entities\Sale;
use App\Entities\Seller;
use Illuminate\Database\Eloquent\Factories\Factory;

class SaleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Sale::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'seller_id' => Seller::factory(),
            'value' => $this->faker->numberBetween(1, 9000),
            'base_commission' => $this->faker->numberBetween(1, 2),
            'commission' => $this->faker->numberBetween(1, 9000),
        ];
    }
}
