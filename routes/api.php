<?php

/**
 * Lib default
 */

use Illuminate\Support\Facades\Route;

/**
 * Actions
 */
use App\Actions\User\SaveUser;
use App\Actions\User\LoginUser;
use App\Actions\User\LogoutUser;
use App\Actions\Seller\SaveSeller;
use App\Actions\Seller\GetAllSellers;
use App\Actions\Sale\SaveSale;
use App\Actions\Sale\GetSales;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('jwt')->group(function () {
    Route::prefix('sellers')->group(function () {
        Route::post('create', SaveSeller::class);
        Route::get('list', GetAllSellers::class);
    });

    Route::prefix('sales')->group(function () {
        Route::post('create/{sellerId}', SaveSale::class);
        Route::get('list/{sellerId}', GetSales::class);
    });
});

Route::post('users/create', SaveUser::class);
Route::post('login', LoginUser::class);
